Go server

## Available Scripts

In the project directory, you can run:

### `dev_appserver.py app.yaml`

Runs the app in the development mode.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

## More

[Google Site](https://cloud.google.com/appengine/docs/standard/go/building-app/creating-your-application)

### deploy

```
gcloud app deploy --project my-memo-site
```

### check

```
gcloud app browse --project my-memo-site
```
