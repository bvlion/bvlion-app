package main

import (
	"github.com/labstack/echo"
	"net/http"
	"google.golang.org/appengine"
)

func createMux() *echo.Echo {
	e := echo.New()
	return e
}

func main() {
	e := echo.New()
	http.Handle("/", e)
	e.File("/", "build/index.html")
	appengine.Main()
}